<?php

// file: app/helpers/default_helper.php
// form field validation
function validates_presence_of($post, $field, $field_name = false) {
    global $CONFIG;

    if (!$field_name) {
        $field_name = $field;
    }

    _dump(@$post[$field]);

    if ((!in_array($field, $post)) || (@$post[$field] == '')) {
        $CONFIG['msg']['error'][] = 'Field ' . $field_name . ' can not be empty';
    }
}

function validate_form() {
    global $CONFIG;

    if (count($CONFIG['msg']['error'])) {
        return false;
    }

    return true;
}

// defines the page title. returns bool.
function head($title = '') {
    $title = $title == '' ? '' : $title . ' - ';
    echo '<title>' . $title . SYSTEM_NAME . '</title>';
    include (DOCROOT . '/app/views/templates/head.php');
    return true;
}

// returns PDO object
function get_pdo() {
    $app = new App_Model();
    return $app->connect();
}

function get_template($template, $params = array()) {
    // $_language_file = DOCROOT.DS.'app'.DS.'languages'.DS.LANGUAGE.'.php';
    // if (file_exists($_language_file)) {
    // 	include ($_language_file);
    // }
    // else {
    // 	die('Language file not found');
    // }
    // extract params
    if ($params) {
        extract($params);
        unset($params);
    }
    include(DOCROOT . '/app/views/templates/' . $template . '.phtml');
}

// display messages from $CONFIG['msg']. retrns nothing.
function display_messages() {
    global $CONFIG;
    if (count($CONFIG['msg']['success'])) {
        echo '<div class="alert alert-success alert-dismissable">';
        echo '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>';
        echo '<h4><i class="icon fa fa-check"></i> Sucesso!</h4>';
        foreach ($CONFIG['msg']['success'] as $msg) {
            echo "<p>{$msg}</p>";
        }
        echo '</div>';
    }
    if (count($CONFIG['msg']['error'])) {
        echo '<div class="alert alert-danger alert-dismissable">';
        echo '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>';
        echo '<h4><i class="icon fa fa-ban"></i> Erro!</h4>';
        foreach ($CONFIG['msg']['error'] as $msg) {
            echo "<p>{$msg}</p>";
        }
        echo '</div>';
    }
    if (count($CONFIG['msg']['info'])) {
        echo '<div class="alert alert-info alert-dismissable">';
        echo '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>';
        echo '<h4><i class="icon fa fa-info"></i> Info!</h4>';
        foreach ($CONFIG['msg']['info'] as $msg) {
            echo "<p>{$msg}</p>";
        }
        echo '</div>';
    }
    if (count($CONFIG['msg']['alert'])) {
        echo '<div class="alert alert-warning alert-dismissable">';
        echo '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>';
        echo '<h4><i class="icon fa fa-warning"></i> Atencao!</h4>';
        foreach ($CONFIG['msg']['alert'] as $msg) {
            echo "<p>{$msg}</p>";
        }
        echo '</div>';
    }
}

// throws error page. returns void.
function error_page($error_code = '404') {
    $file = DOCROOT . '/app/views/errors/' . $error_code . '.php';
    if (file_exists($file)) {
        include ($file);
        exit();
    }
}

// generates strong random id. returns int.
function crypto_rand_secure($min, $max) {
    $range = $max - $min;
    if ($range < 0)
        return $min; // not so random...
    $log = log($range, 2);
    $bytes = (int) ($log / 8) + 1; // length in bytes
    $bits = (int) $log + 1; // length in bits
    $filter = (int) (1 << $bits) - 1; // set all lower bits to 1
    do {
        $rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes)));
        $rnd = $rnd & $filter; // discard irrelevant bits
    } while ($rnd >= $range);
    return $min + $rnd;
}

// generates unique token. returns string.
function get_token($length = 22, $upper = true) {
    $token = "";
    //$codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    $codeAlphabet = "abcdefghijklmnopqrstuvwxyz";
    $codeAlphabet.= "123456789";
    for ($i = 0; $i < $length; $i++) {
        $token .= $codeAlphabet[crypto_rand_secure(0, strlen($codeAlphabet))];
    }
    if ($upper)
        $token = strtoupper($token);
    return $token;
}

// redirects to url. returns void.
function redirect_to($url) {
    header('location:' . WWWROOT . '/' . $url);
}

// manages pages for logged user needs. returns void.
function auth($op = '', $redirect_to = '') {
    switch ($op) {
        case 'yes':
            if (!@$_SESSION['app']['user']) {
                if (!isset($_COOKIE['user_id'])) {
                    redirect_to('logout');
                } else { // log the user back in
                    $db = new Sql(get_pdo());
                    $db->array_only = true;
                    $db->select("*");
                    $db->from("Users");
                    $db->where("id = '" . $_COOKIE['user_id'] . "'");
                    $result = $db->run();
                    if (isset($result)) {
                        $_SESSION['app'] = array(
                            'user' => array(
                                'id' => $result[0]['id'],
                                'name' => $result[0]['name'],
                                'email' => $result[0]['email'],
                            )
                        );
                        //redirect_to('projects');
                    } else {
                        redirect_to('logout');
                    }
                }
            }
            break;
        case 'no':
            if (@$_SESSION['app'])
                redirect_to($redirect_to);
            break;
        default:
            break;
    }
}

// gives access control by role to the page
// assumes user is logged in
function access_control($role) {
    switch ($role) {
        case 'admin':
            if (!@$_SESSION['app']['company']['admin'] != '1') {
                redirect_to();
            }
            break;
        default:
            # code...
            break;
    }
}

// search arrays recursevely. returns bool.
function array_rsearch($needle, $haystack) {
    foreach ($haystack as $key => $value) {
        $current_key = $key;
        if ($needle === $value OR ( is_array($value) && array_rsearch($needle, $value) !== false)) {
            return $current_key;
        }
    }
    return false;
}

// generates class name. returns string.
function get_class_name($string) {
    $inf = new Inflector();
    $plural = ucwords($inf->pluralize($string));
    return $plural . '_model';
}

function get_months($abbr = false) {
    if (!$abbr)
        return array('1' => 'Janeiro', '2' => 'Fevereiro', '3' => 'Março', '4' => 'Abril', '5' => 'Maio', '6' => 'Junho', '7' => 'Julho', '8' => 'Agosto', '9' => 'Setembro', '10' => 'Outubro', '11' => 'Novembro', '12' => 'Dezembro');
    else
        return array('1' => 'Jan', '2' => 'Fev', '3' => 'Mar', '4' => 'Abr', '5' => 'Mai', '6' => 'Jun', '7' => 'Jul', '8' => 'Ago', '9' => 'Set', '10' => 'Out', '11' => 'Nov', '12' => 'Dez');
}

function get_styles() {
    $styles = array(
        'Blues', 'Country', 'Música Eletrônica', 'Folk', 'Gospel', 'Jazz', 'Hip Hop/Rap',
        'Reggae', 'Heavy Metal', 'Hard Rock', 'Indie', 'Emocore', 'Punk Rock', 'Grunge',
        'Pop Rock', 'Pop', 'Música Latina', 'MPB', 'Rock and Roll', 'Rockabilly',
        'Funk', 'Hardcore', 'Sertanejo', 'Sertanejo Universitário', 'Brega', 'Soul', 'Forró', 'Axé',
        'Black Music', 'Bolero', 'Bossa Nova', 'Gótico', 'Infantil', 'Instrumental',
        'Jovem Guarda', 'Latino', 'Pagode', 'Ska'
    );
    $styles['1000'] = 'Não definido';
    asort($styles);
    return $styles;
}

function format_money($value, $symbol = 'R$ ') {
    return $symbol . number_format($value, 2, ',', '.');
}

function doMask($mask,$str){

    $str = str_replace(" ","",$str);

    for($i=0;$i<strlen($str);$i++){
        $mask[strpos($mask,"#")] = $str[$i];
    }

    return $mask;

}

function send_email($subject, $body, $to) {
    $mail = new PHPMailer;

    $mail->isSMTP();                                      // Set mailer to use SMTP
    $mail->Host = 'smtp.mandrillapp.com';  // Specify main and backup SMTP servers
    $mail->SMTPAuth = true;                               // Enable SMTP authentication
    $mail->Username = 'cicerogeorge@gmail.com';                 // SMTP username
    $mail->Password = '4GglbQ9spLxYzUtM9qF4Yw';                           // SMTP password
    $mail->SMTPSecure = 'tls';                            // Enable encryption, 'ssl' also accepted
    $mail->Port = 587;
    $mail->CharSet = 'UTF8';

    $mail->From = 'sistema@audiosort.com';
    $mail->FromName = 'Audiosort';
    $mail->addAddress($to['email'], $to['name']);     // Add a recipient
    $mail->addReplyTo('sistema@audiosort.com', 'Não Responda');

    $mail->WordWrap = 50;                                 // Set word wrap to 50 characters
    $mail->isHTML(true);                                  // Set email format to HTML

    $mail->Subject = $subject;
    $mail->Body = $body;

    if (!$mail->send()) {
        // echo 'Message could not be sent.<br>';
        // echo 'Mailer Error: ' . $mail->ErrorInfo;
        return false;
    } else {
        // echo 'Message has been sent';
        return true;
    }
}

// receives array
// returns string
function get_user_picture($user, $class = '', $css = '') {
    if (strstr(@$user['avatar'], 'facebook.com')) {
        $path = $user['avatar'];
    } else if (!@$user['avatar']) {
        $path = WWWROOT . '/webroot/att/users/user-placeholder.jpg';
    } else {
        $path = WWWROOT . '/webroot/att/users/' . $user['avatar'];
    }
    return '<img src="' . $path . '" class="' . $class . '" style="' . $css . '">';
}

// receives array
// returns string
function get_client_picture($client, $class = '', $css = '') {
    if (strstr(@$client['picture'], 'facebook.com')) {
        $path = $client['picture'];
    } else if (!@$client['picture']) {
        $path = WWWROOT . '/webroot/att/users/user-placeholder.jpg';
    } else {
        $path = WWWROOT . '/webroot/att/clients/' . @$client['picture'];
    }
    return '<img src="' . $path . '" class="' . $class . '" style="' . $css . '">';
}

// sets messages to the system
function add_message($type, $content) {
    global $CONFIG;
    $CONFIG['msg'][$type][] = $content;
}

// send sms confirmation
function send_sms_confirmation($id) {
    // return false;
    $pdo = get_pdo();
    $db = new Sql($pdo);
    $db->array_only = true;
    $db->select("TT.*, C.phone, CU.company_id, CO.name AS company_name, CO.sms_confirmation_message, DATE_FORMAT(TT.startdate, '%d/%m') AS day, DATE_FORMAT(TT.starttime, '%H:%i') AS time, CO.name AS company_name");
    $db->from("Timetables", "TT");
    $db->join("Clients C", "C.id = TT.client_id");
    $db->join("Companies_users CU", "CU.id = TT.companies_user_id");
    $db->join("Companies CO", "CO.id = CU.company_id");
    $db->where("TT.id = '$id'");
    $db->where("TT.sms_confirmation = '1'");
    $db->where("TT.parent_timetable = '0'"); // can't be child, to avoid sending twice
    $db->limit(1);
    if (!$tt = $db->run())
        return false;
    // var_dump($tt);
    $sms = new SMS();
    $message = "Takk for din bestilling. velkommen til " . $tt[0]['company_name'] . " " . format_date($tt[0]['startdate'] . ' ' . $tt[0]['starttime'], 'd/m H:i') . " som avtalt.";
    // if ($tt[0]['sms_confirmation_message'] != '') {
    // 	$message = $tt[0]['sms_confirmation_message'];
    // }
    // else
    if ($tt[0]['sms_confirmation_message'] != '') {
        $message_arr = explode(" ", $tt[0]['sms_confirmation_message']);
        $message = $sep = '';
        // search for variable words
        // possible custom words:
        // day, time, company_name
        foreach ($message_arr as $word) {
            if (substr($word, 0, 1) == '%') {
                $message .= $sep . @$tt[0][substr($word, 1)];
            } else {
                $message .= $sep . $word;
            }
            $sep = ' ';
        }
    }
    // print_r($message);
    // $message = "Hi, you have an appointment: ".format_date($tt[0]['startdate'].' '.$tt[0]['starttime'], 'd/m/Y H:i');
    $number_list = array($tt[0]['phone']);
    // print_r($number_list);
    $response = $sms->send($message, $number_list);
    $json_response = json_encode($response);

    // save sms history
    $__sql = "INSERT INTO messages VALUES(null, '" . $tt[0]['company_id'] . "', '$id', null, '$message', '" . $tt[0]['phone'] . "', '$json_response', '" . now() . "', null, null)";
    $pdo->exec($__sql);
}

function get_http_codes() {
    return array(
        100 => 'Continue',
        101 => 'Switching Protocols',
        102 => 'Processing',
        200 => 'OK',
        201 => 'Created',
        202 => 'Accepted',
        203 => 'Non-Authoritative Information',
        204 => 'No Content',
        205 => 'Reset Content',
        206 => 'Partial Content',
        207 => 'Multi-Status',
        300 => 'Multiple Choices',
        301 => 'Moved Permanently',
        302 => 'Found',
        303 => 'See Other',
        304 => 'Not Modified',
        305 => 'Use Proxy',
        306 => 'Switch Proxy',
        307 => 'Temporary Redirect',
        400 => 'Bad Request',
        401 => 'Unauthorized',
        402 => 'Payment Required',
        403 => 'Forbidden',
        404 => 'Not Found',
        405 => 'Method Not Allowed',
        406 => 'Not Acceptable',
        407 => 'Proxy Authentication Required',
        408 => 'Request Timeout',
        409 => 'Conflict',
        410 => 'Gone',
        411 => 'Length Required',
        412 => 'Precondition Failed',
        413 => 'Request Entity Too Large',
        414 => 'Request-URI Too Long',
        415 => 'Unsupported Media Type',
        416 => 'Requested Range Not Satisfiable',
        417 => 'Expectation Failed',
        418 => 'I\'m a teapot',
        422 => 'Unprocessable Entity',
        423 => 'Locked',
        424 => 'Failed Dependency',
        425 => 'Unordered Collection',
        426 => 'Upgrade Required',
        449 => 'Retry With',
        450 => 'Blocked by Windows Parental Controls',
        500 => 'Internal Server Error',
        501 => 'Not Implemented',
        502 => 'Bad Gateway',
        503 => 'Service Unavailable',
        504 => 'Gateway Timeout',
        505 => 'HTTP Version Not Supported',
        506 => 'Variant Also Negotiates',
        507 => 'Insufficient Storage',
        509 => 'Bandwidth Limit Exceeded',
        510 => 'Not Extended',
        601 => 'Custom Status',
        602 => 'Custom Status',
        603 => 'Custom Status',
        604 => 'Custom Status',
        605 => 'Custom Status',
    );
}

function exit_status($str) {
    echo json_encode(array('status' => $str));
    exit;
}

function get_extension($file_name) {
    $ext = explode('.', $file_name);
    $ext = array_pop($ext);
    return strtolower($ext);
}

function _dump($var, $die= false) {
    echo '<pre style="z-index: 9999; display: block;">';
    var_dump($var);
    echo '</pre>';
    if ($die) die();
}

function get_weekdays($short = true) {
    if ($short) {
        $weekdays = array('1' => 'Mon', '2' => 'Tue', '3' => 'Wed', '4' => 'Thu', '5' => 'Fri', '6' => 'Sat', 'Sun');
    } else {
        $weekdays = array('1' => 'Monday', '2' => 'Tuesday', '3' => 'Wednesday', '4' => 'Thursday', '5' => 'Friday', '6' => 'Saturday', 'Sunday');
    }

    return $weekdays;
}

function get_user_avatar($avatar, $class = '', $css = '') {
    if (!$avatar) {
        return '<img src="' . WEBROOT . '/att/users/user-placeholder.jpg" class="' . $class . '" style="' . $css . '" />';
    } else if (preg_match("/(.{1,})(facebook.com)(.{1,})/", $avatar)) {
        return '<img src="' . $avatar . '" class="' . $class . '" style="' . $css . '" />';
    } else {
        return '<img src="' . WEBROOT . '/' . $avatar . '" class="' . $class . '" style="' . $css . '" />';
    }
}

function print_timetable_status($tt) {
    if ($tt['deleted_at']) {
        return '<span class="label label-important">Canceled at ' . format_date($tt['deleted_at'], 'd/m/Y') . '</span>';
    } else {
        return '<span class="label label-success">Active</span>';
    }
}

function print_timetable_presence($tt) {
    if ($tt['present'] != '1') {
        return '<span class="label label-important">Nei</span>';
    } else {
        return '<span class="label label-success">Ja</span>';
    }
}

function core_errors($e) {
    // _dump($e);
    global $core_errors;
    if (is_object($e)) {
        $trace = $e->getTrace();
        $html = '<div class="core-error">DEVELOPMENT MODE - CORE ERROR:<br>';
        $html .= $e->getMessage() . '<br />';
        $html .= $trace[1]['file'] . '<br />';
        $html .= 'Line ' . $trace[1]['line'] . '<br />';
        $html .= '</div>';

        echo $html;
    }
}

function firstName($name) {
    if (strstr($name, ' ')) {
        return substr($name, 0, strpos($name, ' '));
    } else {
        return $name;
    }
}

function getUrl($url, $params = '') {
    $sep = '/';
    $get = '';

    // if any parameters come
    if ($params) {
        $get = '/';
    }

    // if $_GET params
    if (strstr($params, '?')) {
        $get = '';
    }

    return WWWROOT . $sep . $url . $get . $params;
}

// filter values to insert into database
function filterVar($value, $type) {
    switch ($type) {
        case 'float':
        case 'double':
            if (preg_match("/^(([0-9]{1,3}\.){1,5})?([0-9]{1,3}\,[0-9]{2})$/", $value)) {
                $value = str_replace('.', '', $value);
                $value = str_replace(',', '.', $value);
            }
            break;
        default:
            break;
    }
    return $value;
}

function count_services($group_id) {
    $db = new Sql(get_pdo());
    $db->select("COUNT(*) AS qtd");
    $db->from("Companies_services");
    $db->where("companies_group_id = '$group_id'");
    if ($result = $db->run()) {
        return $result->qtd;
    } else {
        return 0;
    }
}

// returns start and end time for employee agenda on a specific date
// date format Y-m-d
function get_employee_period($companies_user_id, $date) {
    $db = new Sql(get_pdo());
    $db->array_only = true;
    $db->select("*");
    $db->from("Timetables", "TT");
    $db->where("TT.companies_user_id = '$companies_user_id'");
    $db->where("TT.startdate = '$date'");
    $db->where("TT.internal = '1'");
    $db->where("TT.deleted_at IS NULL");
    $db->limit(1);
    if ($result = $db->run()) {
        return $result[0];
    }
    return false;
}

function get_params_from_csv($csv_row, $csv_fields) {
    $params = array();
    foreach ($csv_fields as $key => $value) {
        if (!array_key_exists($value, $csv_row)) {
            $params[$key] = '';
        } else {
            $params[$key] = trim($csv_row[$value]);
        }
    }
    return $params;
}

function printTaskStatus($status) {
    global $CONFIG;
    $_status = $CONFIG['status'];
    switch ($status) {
        case '0':
        case '1':
            $class = 'info';
            break;
        case '2':
            $class = 'primary';
            break;
        case '3':
            $class = 'success';
            break;
        case '4':
            $class = 'default';
            break;
        case '5':
            $class = 'danger';
            break;
        case '6':
            $class = 'warning';
            break;
        default:
            $class = 'default';
            break;
    }

    return '<span class="label label-' . $class . '">' . $_status[$status] . '</span>';
}

function printTaskChanges($item) {
    global $CONFIG;
    $html = $item['owner'] . ' changed ';
    if ($item['task_value'] == '') {
        $item['task_value'] = '<em>NULL</em>';
    }
    switch ($item['task_field']) {
        case 'user_id':
            // get new task owner
            $db = new Sql(get_pdo());
            $db->select("*");
            $db->from("Users");
            $db->where("id = '" . $item['task_value'] . "'");
            $db->limit(1);
            $user = $db->run();
            $html .= "<strong>Owner</strong> to <strong>" . $user->name . "</strong>";
            break;
        case "title";
            $html .= "<strong>Title</strong> to <strong>" . $item['task_value'] . "</strong>";
            break;
        case "about";
            $html .= "<strong>About</strong> to <strong>" . substr($item['task_value'], 0, 30) . "...</strong>";
            break;
        case "progress":
            $html .= "<strong>Progress</strong> to <strong>" . $item['task_value'] . "%</strong>";
            break;
        case "status":
            $html .= "<strong>Status</strong> to <strong>" . $CONFIG['status'][$item['task_value']] . "</strong> (dues at " . format_date($item['due_date'], 'd/m/Y') . ")";
            break;
        case "tracker":
            $html .= "<strong>Tracker</strong> to <strong>" . $CONFIG['tracker'][$item['task_value']] . "</strong>";
            break;
        case "priority":
            $html .= "<strong>Priority</strong> to <strong>" . $CONFIG['priority'][$item['task_value']] . "</strong>";
            break;
        case "start_date":
            $html .= "<strong>Start Date</strong> to <strong>" . format_date($item['task_value'], 'd/m/Y') . "</strong>";
            break;
        case "due_date":
            $html .= "<strong>Due Date</strong> to <strong>" . format_date($item['task_value'], 'd/m/Y') . "</strong>";
            break;
        case "due_date":
            $html .= "<strong>Due Date</strong> to <strong>" . format_date($item['task_value'], 'd/m/Y') . "</strong>";
            break;
        case "attachment":
            $html .= "<strong>Attachment</strong>";
            break;
        default:
            $html .= "<strong>" . $item['task_field'] . "</strong> to <strong>" . $item['task_value'] . "</strong>";
            break;
    }


    return $html;
}

function printRowClassTaskStatus($status, $delayed) {
    if ($status == 3) {
        return '';
    }
    if ($delayed < 0) {
        return 'class="task-delayed"';
    } else if ($delayed == 0) {
        return 'class="task-due"';
    }
    return '';
}

function printFileFormatIcon($extension) {
    switch ($extension) {
        case 'docx':
        case 'doc':
        case 'txt':
            return '<span class="file file-doc"></span>';
        case 'xls':
        case 'xlsx':
            return '<span class="file file-xsl"></span>';
        case 'pdf':
            return '<span class="file file-pdf"></span>';
        case 'zip':
            return '<span class="file file-zip"></span>';
        default:
            return '<span class="file file-default"></span>';
    }
}

function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function generateHexToken($length = 10) {
    $characters = '0123456789abcdef';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function isActive($op) {
    if ($op) {
        return '<span class="label label-success">Sim</span>';
    }
    else {
        return '<span class="label label-danger">Nao</span>';
    }
}

function format_phone($number){
    $ddd = $number[0].$number[1];

    $part1 = $number[2].$number[3].$number[4].$number[5].$number[6];
    $part2 = $number[7].$number[8].$number[9].$number[10];

    $number = "(".$ddd.") ".$part1."-".$part2;
    return $number;
}