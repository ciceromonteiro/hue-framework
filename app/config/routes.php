<?php
// file: app/config/routes.php

$routes = array();

// default route
$routes['default'] = 'session/login';

$routes['login'] = 'session/login';
$routes['logout'] = 'session/logout';