<?php
// file: app/config/config.php
define('SYSTEM_NAME', '');
if ($_SERVER['SERVER_NAME'] == 'localhost' || $_SERVER['SERVER_NAME'] == '127.0.0.1' || strstr($_SERVER['SERVER_NAME'], '192.168'))
{
	define('DIR', '');
	// database config
	define('HOST', '127.0.0.1');
	define('DATABASE', '');
	define('USERNAME', '');
	define('PASSWORD', '');
	define('PREFIX', '');
	define('PORT', '');
}
else
{
	define('DIR', '');
	// database config
	define('HOST', '');
	define('DATABASE', '');
	define('USERNAME', '');
	define('PASSWORD', '');
	define('PREFIX', '');
	define('PORT', '');
}

// system language
// available: en, no
define('LANGUAGE', '');

include('mimes.php');
