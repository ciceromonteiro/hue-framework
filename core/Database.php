<?php
// file: core/Database.php

class Database {

	protected $host;
	protected $login;
	protected $pswd;
	protected $database;

	function __construct() {
		$this->host = HOST;
		$this->database = DATABASE;
		$this->login = USERNAME;
		$this->pswd = PASSWORD;
	}

	public function set($param, $value) {
		$this->$param = $value;
	}

	// connects to the database
	// returns PDO or false
	public function connect($driver='mysql') {
		$pdo = new PDO($driver.":host=".HOST.";dbname=".DATABASE, USERNAME, PASSWORD);
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $pdo->exec("SET CHARACTER SET utf8");

        $this->host = $this->database = $this->login = $this->pswd = null;

		return $pdo;
	}

}
