<?php

// file: core/App_Model.php

class App_Model extends Database {

    protected $sql;
    protected $inf;
    protected $pdo;

    function __construct() {
        $this->inf = new Inflector();
        $this->sql = '';
        $this->pdo = $this->connect();
        parent::__construct();
    }

    public function get_by_id($id) {
        return $this->get_by(array('id' => $id));
    }

    public function get_by($params = array()) {
        $class = get_class($this);
        if (!$obj = $this->retrieve($params))
            return false;

        foreach ($obj[0] as $key => $value) {
            if (property_exists($class, $key)) {
                $this->$key = $value;
            }
        }
        // $this->sql = $this->inf = $this->pdo = null;
        return $this;
    }

    public function set($param, $value) {
        $this->$param = $value;
    }

    public function set_values($params) {
        // set values to object
        foreach ($params as $key => $value) {
            $this->$key = $value;
        }
    }

    public function _table_name($str) {
        return strtolower($this->inf->pluralize($str));
    }

    public function validates_fields($params, $mandatory) {
        global $CONFIG;
        $return = true;
        foreach ($mandatory as $key => $field) {
            if (@$params[$key] == '') {
                $CONFIG['msg']['error'][] = 'O campo ' . $field . ' é obrigatório';
                $return = false;
            }
        }
        return $return;
    }

    // retrieves any object from the database
    // sql injection safe, can receive params directly from user
    public function retrieve($params = array(), $order_by = '', $limit = '', $active_only = false) {

        $pdo = $this->connect();

        $class = get_class($this);

        $_where = '';
        $and = '';

        if ($active_only) {
            $_where .= " deleted_at IS NULL ";
            $and = 'AND ';
        }

        $table = $this->_table_name(substr($class, 0, strpos($class, '_Model')));

        $sql = "SELECT * FROM $table";

        if (is_array($params) && count($params)) {
            foreach ($params as $key => $value) {
                if ($value === 'null') {
                    $_where .= $and . $key . " IS NULL";
                    unset($params[$key]);
                }
                else if (strtolower($value) === 'not null') {
                    $_where .= $and . $key . " IS NOT NULL";
                    unset($params[$key]);
                }
                else if (strstr($value, '!=')) {
                    $params[$key] = trim(str_replace('!=', '', $value));
                    $_where .= $and . $key . " != ?";
                }
                else {
                    $_where .= $and . $key . " = ?";
                }
                $and = ' AND ';
            }
        }

        $sql .= $_where ? " WHERE $_where" : "";

        $sql .= $order_by != '' ? " ORDER BY $order_by" : '';
        $sql .= $limit != '' ? " LIMIT $limit" : '';

        $values = is_array($params) ? array_values($params) : array();

        // _dump($sql);
        // _dump($values);
        // die();

        $rs = $pdo->prepare($sql);
        if ($rs->execute($values)) {
            $result = $rs->fetchAll(PDO::FETCH_ASSOC);
            if (!count($result))
                return false;
            return $result;
        }

        return false;
    }

    public function create($debug = 0) {

        global $CONFIG;

        $obj = $this;

        $fields_insert = $values_insert = array();
        // get object class
        $class = strtolower(get_class($obj));
        // get database table name
        $table = $this->_table_name(substr($class, 0, strpos($class, '_model')));
        // connect to database
        $pdo = $this->connect();
        // set created date
        $obj->created_at = date('Y-m-d H:i:s');
        // transform object into array
        $obj_arr = (array) $obj;
        // filter object values
        foreach ($obj_arr as $key => $value) {
            // avoid internal objects
            if (is_object($value))
                continue;
            // filter float
            if (preg_match("/^([0-9]\.?){1,}\,([0-9]{1,})$/", $value)) {
                $value = str_replace('.', '', $value);
                $value = str_replace(',', '.', $value);
                $obj_arr[$key] = $value;
            }
            // filter date
            else if (preg_match("/^([0-9]{2}\/){2}([0-9]{4})$/", $value)) {
                $value = format_date($value, 'Y-m-d');
                $obj_arr[$key] = $value;
            }
        }
        $_sql_log = '';
        // iterate for reference fields for sql string
        foreach ($obj_arr as $field => $value) {
            $fields_insert[] = '`'.$field.'`';
            $values_insert[] = ':' . $field;
            // avoid getting extended vars
            if ($field == 'deleted_at')
                break;
        }
        $fields_insert = implode(',', $fields_insert);
        $values_insert = implode(',', $values_insert);
        // mount sql string
        $sql = "INSERT INTO $table($fields_insert) VALUES($values_insert)";

        $rs = $pdo->prepare($sql);
        // set reference values into PDOStatement object
        foreach ($obj_arr as $f => $v) {
            $rs->bindValue(':' . $f, $v);
            // avoid getting extended vars
            if ($f == 'deleted_at')
                break;
        }

        try {
            $rs->execute();
            if ($debug) {
                $rs->debugDumpParams();
            }
            $id = $pdo->lastInsertId();
            // insert into log
            // $_sql_log = "INSERT INTO system_logs(
            //     user_id, class, class_id, operation, json_fields, json_values, created_at
            // ) VALUES(
            //     '".@$_SESSION['app']['user']['id']."', '$class', '$id', 'create', '".json_encode($fields_insert)."', '".json_encode($fields_values)."', '".date('Y-m-d H:i:s')."');";
            // $pdo->exec($_sql_log);
            return $id;
        } catch (PDOException $e) {
            if ($debug) {
                $rs->debugDumpParams();
                echo $sql;
                core_errors($e);
            }
            return false;
        }
    }

    public function update($debug=false) {

        $class = strtolower(get_class($this));
        // get database table name
        $table = $this->_table_name(substr($class, 0, strpos($class, '_model')));

        $obj_array = (array) $this;

        $sql = "UPDATE " . $table . " SET ";
        $values_array = array();
        $comma = '';

        foreach ($obj_array as $key => $value) {
            $sql .= $comma . '`'.$key.'`' . ' = ?';
            $comma = ', ';
            $values_array[] = $value;
            if ($key == 'deleted_at')
                break;
        }

        $values_array[] = $this->id;
        $sql .= " WHERE id = ?";

        $pdo = $this->connect();

        $rs = $pdo->prepare($sql);
        // if ($rs->execute($values_array)) {
        // 	return true;
        // }
        // else {
        // 	var_dump($rs->errorInfo());
        // }
        try {
            $rs->execute($values_array);
            return true;
        } catch (PDOException $e) {
            if ($CONFIG['app']['env'] == 'development') {
                core_errors($e);
            }
            if ($debug) {
                _dump($sql);
                _dump($values_array);
                _dump($rs->errorInfo());
            }
            return false;
        }
    }

    public function delete() {
        global $CONFIG;
        $class = get_class($this);
        // get database table name
        $table = $this->_table_name(substr($class, 0, strpos($class, '_Model')));

        $sql = "DELETE FROM $table WHERE id = '{$this->id}'";

        $pdo = $this->connect();

        $rs = $pdo->prepare($sql);

        // if ($rs->execute(array($this->id))) {
        // 	return true;
        // }
        // else {
        // 	var_dump($rs->errorInfo());
        // }

        try {
            $rs->execute(array($this->id));
            return true;
        } catch (PDOException $e) {
            // if ($CONFIG['app']['env'] == 'development') {
            //     core_errors($e);
            // }
            return false;
        }
    }

    // receives array with ids to batch delete from database
    public function delete_batch($collection) {
        $class = get_class($this);

        $table = $this->_table_name(substr($class, 0, strpos($class, '_model')));

        $sql = '';
        foreach ($collection as $id) {
            $sql .= "DELETE FROM $table WHERE id = '$id';";
        }

        $pdo = $this->connect();

        $rs = $pdo->prepare($sql);

        // if ($rs->execute()) {
        // 	return true;
        // }
        // else {
        // 	var_dump($rs->errorInfo());
        // }
        try {
            $rs->execute();
            return true;
        } catch (PDOException $e) {
            if ($CONFIG['app']['env'] == 'development') {
                core_errors($e);
            }
            return false;
        }
    }

}
