<?php
// file: core/AppController.php

class App_Controller extends Inflector{

	function __construct() {
		return $this;
	}

	/**
	* load_() - loads view trhu the route url
	* @param string $type, string $addr
	* @return null
	*/
	public function load($type, $addr, $params=false) {

		switch ($type) {
			case 'view':
				return $this->loadView($addr, $params);
				break;
			case 'model':
				return $this->loadModel($addr);
				break;
			case 'controller':
				return $this->loadController($addr);
				break;
			case 'helper':
				return $this->loadHelper($addr);
				break;
			default:
				break;
		}

	}

	/**
	* loadView() - loads view trhu the route url
	* @param string $view
	* @return mixed
	*/
	private function loadView($view, $params=false) {

		global $urlParams;

		// load language file
		// $_language_file = DOCROOT.DS.'app'.DS.'languages'.DS.LANGUAGE.'.php';
		// if (file_exists($_language_file)) {
		// 	include ($_language_file);
		// }
		// else {
		// 	die('Language file not found');
		// }

		// extract params
		if ($params) {
			extract($params);
			unset($params);
		}

		$_arr = explode('/', $view);
		unset($view);
		// views dir
		$viewDir = DOCROOT.DS.'app'.DS.'views'.DS;
		$viewAddr = $viewDir.$_arr[0].DS.$_arr[1].'.phtml';
		// check if file exists and give an exception
		if (file_exists($viewAddr)) {
			unset($_arr);
			unset($viewDir);
			include($viewAddr);
		} else {
			die('<div class="system-error">View '.$viewAddr.' not found</div>');
		}

	}

	/**
	* loadModel() - loads view trhu the route url
	* @param string $view
	* @return mixed
	*/
	private function loadModel($model) {

		global $urlParams;

		$model = ucwords($this->singularize($model));
		$modelDir = DOCROOT.DS.'app'.DS.'models'.DS;
		$modelAddr = $modelDir.$model.'_model.php';
		// check if file exists and give an exception
		if (file_exists($modelAddr)) {
			if (class_exists(substr($modelAddr, 0, strpos($modelAddr, '.php')))) return false;
			include($modelAddr);
		} else {
			//die('<div class="system-error">View '.$modelAddr.' not found</div>');
		}

	}

	/**
	* loadController() - loads a specific controller
	* @param string $controller
	* @return mixed
	*/
	private function loadController($controller) {

		global $urlParams;

		// load language file
		$_language_file = DOCROOT.DS.'app'.DS.'languages'.DS.LANGUAGE.'.php';
		if (file_exists($_language_file)) {
			include ($_language_file);
		}
		else {
			die('Language file not found');
		}

		$controller = strtolower($this->pluralize($controller));
		$controllerDir = DOCROOT.DS.'app'.DS.'controllers'.DS;
		$controllerAddr = $controllerDir.$controller.'_controller.php';
		// check if file exists and give an exception
		if (file_exists($controllerAddr)) {
			if (class_exists(substr($controllerAddr, 0, strpos($controllerAddr, '.php')))) return false;
			include($controllerAddr);
		} else {
			die('<div class="system-error">Controller '.$controllerAddr.' not found</div>');
		}

	}

	/**
	* loadController() - loads a specific helper
	* @param string $helper
	* @return mixed
	*/
	private function loadHelper($helper) {

		$file = DOCROOT.DS.'app'.DS.'helpers'.DS.$helper.'_helper.php';
		if (file_exists($file))
			include ($file);
	}

	/**
	* returnJson() - return a formatted json string
	* @param string $status_code
	* @param array $items
	* @return mixed
	*/
	protected function returnJson($httpStatus, $items=array(), $extra=array()) {
		$http_codes = get_http_codes();
		$return = array(
			'status' => array(
				'code' => $httpStatus,
				'status' => @$http_codes[$httpStatus]
			),
			'items' => $items,
			'extra' => $extra
		);
		die(json_encode($return, JSON_PRETTY_PRINT));
	}

}
