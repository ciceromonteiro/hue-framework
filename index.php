<?php
// file: index.php

// set default configs
date_default_timezone_set('America/Recife');
header('Content-type: text/html; charset=utf-8');
ini_set('display_errors', 1);// 0 for production
// session handler
session_name('somegreatsecurehashhere');
if (!session_id()) {
	session_start();
}
ob_start();

$includeFiles = array();
$includeFiles['config']     = 'app/config/config.php';
$includeFiles['routes']     = 'app/config/routes.php';
$includeFiles['autoloader'] = 'app/config/autoloader.php';
$includeFiles['globals']    = 'app/config/globals.php';

if (!file_exists($includeFiles['config'])) die('Config file not found: '.$includeFiles['config']);
if (!file_exists($includeFiles['routes'])) die('Routes file not found: '.$includeFiles['routes']);
if (!file_exists($includeFiles['autoloader'])) die('Autoloader file not found: '.$includeFiles['autoloader']);
if (!file_exists($includeFiles['globals'])) die('Globals file not found: '.$includeFiles['globals']);

include ('app/config/config.php');
include ('app/config/autoloader.php');
include ('app/config/globals.php');
include ('app/config/routes.php');

define('DS', DIRECTORY_SEPARATOR);

$DOCUMENT_ROOT = dirname(__FILE__);

define('DOCROOT', $DOCUMENT_ROOT);
define('WWWROOT', "http://".$_SERVER['SERVER_NAME'].DIR);
define('WEBROOT', WWWROOT."/webroot");

// load core helpers
include ('core/helpers/Inflector.php');
include ('core/helpers/Form.php');
include ('core/helpers/File.php');
include ('core/helpers/Sql.php');
include ('core/helpers/PHPMailer/PHPMailerAutoload.php');
// load core classes
include ('core/Database.php');
include ('core/Route.php');
include ('core/App_Controller.php');
include ('core/App_Model.php');

// load app helpers
if (count($autoloader['helpers'])) {
	foreach ($autoloader['helpers'] as $helper) {
		include (DOCROOT.'/app/helpers/'.$helper.'_helper.php');
	}
}

if (@$_GET['dump']) {
	dump($_COOKIE);
	dump($_SESSION);
}

// ROUTE SETTINGS
$rt = new Route();
// apply default routes settings
$self = str_replace('index.php', '', $_SERVER['PHP_SELF']);
// $requestUri = str_replace($self, '', $_SERVER['REQUEST_URI']);
$requestUri = $self == '/' ? substr($_SERVER['REQUEST_URI'], 1) : str_replace($self, '', $_SERVER['REQUEST_URI']);
if (array_key_exists('QUERY_STRING', $_SERVER)) {
	$requestUri = str_replace('?'.$_SERVER['QUERY_STRING'], '', $requestUri);
}

$uri_array = explode('/', $requestUri);
$uri_key = $uri_array[0];
array_shift($uri_array);

if ($requestUri == '') { // default routes settings
	$rt->load($routes['default']);
} else if (array_key_exists($uri_key, $routes)) { // personal routes settings

	$rt->load($routes[$uri_key], $uri_array);
} else { // standard routes
	$rt->load($requestUri);
}
